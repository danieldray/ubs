inputUserName=$1
inputTemplateName=$2
userNameID=`curl --user admin:password http://10.182.160.38/api/v2/users/?username=$inputUserName | jq .results[].id`

templateID=`curl --user admin:password http://10.182.160.38/api/v2/job_templates/?name=$inputTemplateName | jq .results[].id`

executeRoleID=`curl --user admin:password http://10.182.160.38/api/v2/job_templates/$templateID/ | jq .summary_fields.object_roles.execute_role.id`

generate_post_data() 
{
cat << EOF 
    {"id":$executeRoleID} 
EOF
}
curl --user admin:password -d "$(generate_post_data)" -H "Content-Type: application/json" -X POST http://10.182.160.38/api/v2/users/$userNameID/roles/

curl --user admin:password /api/v2/system_job_templates/{id}/schedules/ | jq

$SHELL