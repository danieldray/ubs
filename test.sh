test2=`grep 't118458' extract.txt`

test=`grep 'JOB_ID' extract.txt`

declare -a test_array

echo $test2
if [ "$test2" == "" ] ;
then
    echo "user doesnt exist"
else 
    input=extract.txt
    while IFS= read -r line
    do
        test1=`grep "JOB_ID" | sed 's/.*=//' | sed 's/:.*//'`
        test_array+=( $test1 )
    done < "$input"

    postDataSchedule() 
    {
        cat << EOF 
            {"extra_vars": {"templateArray": "${test_array[@]}" ,"userName": "chrisAPI" ,"templateName": "add_Template_Permissions"}}
EOF
    }
    #ansible-playbook addTemplatePermissions.yml -i 10.182.160.38, -u oraansible -k -v --extra-vars '{"userName":"chrisAPI", "templateName":"add_Template_Permissions"}'
    curl --user admin:password -d "$(postDataSchedule)" -H "Content-Type: application/json" -X POST http://10.182.160.38/api/v2/job_templates/12/launch/
fi